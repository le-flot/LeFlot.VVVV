# Le Flot

Le Flot est un projet artistique, multimédia et interactif sponsorisé par la
ville de Sherbrooke.

# Développement

## Prérequis

Voici l'application VVVV avec les modules préinstallés nécessaire pour rouler le patch:

http://julienrobert.net/leFlot/vvvv.7z

Le répertoire Git doit être placé à l'intérieur du dossier Project de l'application téléchargée.

Une fois décompressé, il faut ouvrir le fichier config.exe pour vérifier que toutes les composantes sont bien installées sur la machine. Installer ce qui manque au besoin.

Au premier démarrage, cliquer sur autoriser à utiliser les réseaux privés et publics.

## Pour démarrer

Il faut démarrer la dernière version du patch Project/ParticleSelfCollide/Le Flot_water_.

Pour que VVVV démarre automatiquement ce patch à l'ouverture, il faut remplacer le fichier root.v4p inclut dans ce dépôt.

Une fois démarré, il faut appuyer sur le bouton Reset en haut à gauche à chaque fois qu'on démarre Unity pour repartir les plugins Spout.
